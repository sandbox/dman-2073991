Field Formatter Order
--------------------------

When adding multiple fields to a content type, the order shown on the edit
form (the delta) is usually used when rendering.

This minor formmatter allows you to adjust that, eg to reverse the order.

Dependencies
------------
Field formatter settings API module.
